import ActionRef from '../vo/ActionRef';

interface GameState{
    gameEndTimeStamp:number,
    phase:"IDLE"|"INTRO"|"GAME"|"END",
    universes:{
        currentPhase:number,
        currentPhaseEndTimeStamp,
        actions:ActionState[],
        done:boolean;
    }[]
    
}

export interface ActionState{
    definition:ActionRef,
    state:{
        done:boolean,
        sequence:string,
        success:boolean,
    }
}

export interface ActionDefinition{
    id:string,
    name:string,
    phaseId:number,
    phaseDuration:number,
    manipulationValue:number,
    actionTexte:string,
    actionLabel:string,
    recoValue:string,
    recoTexte:string,
    remplacementValue:string,
    remplacementTexte:string,
    divers:string,
    diversTexte:string,
    yesNo:boolean,
    bddReco:string,
    validationSound:string,
}


// let defaultGameState:GameState = {

// }

export default GameState;