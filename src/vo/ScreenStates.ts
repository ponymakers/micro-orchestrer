import Universe from './Universe';
import PubSub from '../controllers/PubSub';
import ActionRef from './ActionRef';

    class ScanState{
        phase:"enigme1"|"enigme2"|"game"|"idle"|"video" = "idle";
        scanEnabled:boolean = false;;
        scanning:boolean = false;
        pins:{actionId:string, x:number, y:number, color:string}[] = [];
        message?:string = "";
        timestamp:number = 0;
    }


    class DbState{
        phase:"enigme1"|"enigme2"|"game"|"idle"|"message" = "idle";
        action?:ActionRef;
        message:string = "";
    }

    class CoffreState{
        phase:"locked"|"on"|"video"|"message" = "message";
        timestamp:number = 0;
        trophy?:string;
        jaugeValue:number = 100;
        jaugeMax:number = 100;
        scanActive:boolean = false;
        message:string = "Vérouillé";
    }

    class ThermostatState{
        phase:"sleep"|"temperature"|"programmation" = "sleep";
        temperature:number = 0;
    }


    class PCState{
        phase:"sleep"|"temperature"|"programmation" = "sleep";
        temperature:number = 0;
    }

    class TVState{
        phase:"video"|"sleep" = "video";
    }


    export default class ScreenStates 
    {
        private _scans:ScanState[] = [
            new ScanState(),
            new ScanState()
        ]

        private _dbs:DbState[] = [
            new DbState(),
            new DbState(),
        ]

        private _coffres:CoffreState[] = [
            new CoffreState(),
            new CoffreState(),
        ]

        getScan(id:Universe){
            return this._scans[id];
        }

        getDb(id:Universe){
            return this._dbs[id];
        }

        getCoffre(id:Universe){
            return this._coffres[id];
        }

        setScan(id:Universe, action:(ScanState:ScanState)=>ScanState){
            this._scans[id] = action(this._scans[id]);;
            PubSub.pub("/states/scan/"+id, JSON.stringify(this._scans[id]));
        }

        setDb(id:Universe, action:(DbState:DbState)=>DbState){
            this._dbs[id] = action(this._dbs[id]);
            PubSub.pub("/states/db/"+id, JSON.stringify(this._dbs[id]));
        }

        setCoffre(id:Universe, action:(CoffreState:CoffreState)=>CoffreState ){
            this._coffres[id] = action(this._coffres[id]);
            PubSub.pub("/states/coffre/"+id, JSON.stringify(this._coffres[id]));
        }

        
        // private _thermostats:ThermostatState[] = [
        //     new ThermostatState(),
        //     new ThermostatState(),
        // ]

        // private _pc:PCState = new PCState;

        // private _tv:TVState = new TVState;


        // getThermostat(id:Universe){
        //     return this._thermostats[id];
        // }

        // setThermostat(id:Universe, value:ThermostatState){
        //     this._thermostats[id] = value;
        //     PubSub.pub("/states/thermostat/"+id, JSON.stringify(value));
        // }

        // get pc(){
        //     return this._pc;
        // }
        
        // set pc(value = this.pc){
        //     this._pc;
        //     PubSub.pub("/states/thermostat/"+id, JSON.stringify(value));
        // }
        

    }
