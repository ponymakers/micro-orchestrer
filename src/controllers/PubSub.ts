import * as mqtt from 'mqtt'
import conf from '../config/conf';
import Logger from '../utils/Logger';

export interface MqttEvent{
    topic:string;
    payload:string;
    props:any;
    listenerTopic:string|RegExp;
}

class PubSub{
    _client?:mqtt.Client
    _handlers:{topic:string, handler:(event:MqttEvent)=>any}[] = [];
    _handlersAll:((event:MqttEvent)=>any)[] = [];    
    _handlerConnected:(()=>void)[] = [];

    
    get client():mqtt.Client{
        return this._client ? this._client : this.init();
    }

    init():mqtt.Client{
        this._client = mqtt.connect(conf.mqttBrokerUrl);
        
        this._client.on("connect", (status:any)=>{
            console.log("MQTT connected")
            for(let handler of this._handlerConnected){
                handler();
            }
        })
        
        this._client.on("message", (topic:string, message:string)=>{
            this.handleMessage(topic, message)
        })

        return this._client;
    };

    connected(handler:()=>void){
        this._handlerConnected.push(handler)
    }
    handleMessage(topic:string, message:string){
        let handled:boolean = false;
        Logger.log("MQTT received : ", topic, "with message", message.toString().substring(0,30))

        let event:MqttEvent = {
            topic,
            payload:message.toString(),
            props:{},
            listenerTopic:""
        }

        for (const handler of this._handlersAll) {
            console.log(this._handlersAll)
            event.listenerTopic="#";
            handler(event)
        }

        for (const handler of this._handlers) {
            let messageSubTopics = topic.split("/");
            let handlerSubTopics = handler.topic.split("/");
            if(handlerSubTopics.length != messageSubTopics.length){
                continue;
            }
            let match = true;
            let props:any = {};
            for(let i=0; i<handlerSubTopics.length; i++){
                let messageSubTopic = messageSubTopics[i];
                let handlerSubTopic = handlerSubTopics[i];

                if(handlerSubTopic.startsWith(":")){
                    props[handlerSubTopic.substr(1)] = messageSubTopic;
                    continue;
                }
                if(messageSubTopic == handlerSubTopic){
                    continue;
                }
                match = false;
                break;
            }

            if(match){
                event.props = props;
                event.listenerTopic = handler.topic;

                handler.handler(event);
                handled = true;
            }
        }

        if(!handled){
            //Logger.warn("MQTT Unhandled topic", topic, "with message", message.toString())
        }
        
    }
    
    sub(topic:string, handler:(event:MqttEvent)=>any){
        this._handlers.push({topic, handler});
        if(this._client){ 
            let formattedTopic = topic.split("/")
                    .map((subTopic)=>{
                        return subTopic.startsWith(":") ? "+" : subTopic
                    }).join("/");
                    Logger.log("MQTT sub : ", topic, ":", "["+formattedTopic+"]")
            this._client.subscribe(formattedTopic);
        }
    }

    subAll(handler:(event:MqttEvent)=>any){
        this._handlersAll.push(handler);
        if(this._client){
            this._client.subscribe("#");
        }
    }

    pub(topic:string, payload:string = ""){
        if(this._client){
            Logger.log("MQTT publish : ", topic, ":", "["+payload.substring(0,30)+"]")
            this._client.publish(topic, payload || "");
        }
        else{
            Logger.warn("MQTT Message not send : ", topic, "with message", payload)
        }
    }
}

export default new PubSub();
