

export default class Logger{
    static debug(message?:any, ...optionnalParameters:any[]){
        console.log(message,optionnalParameters)
    };
    static log(message?:any, ...optionnalParameters:any[]){
        console.log(message, ...optionnalParameters)
    };

    static error(message?:any, ...optionnalParameters:any[]){
        console.error(message, ...optionnalParameters)
    };

    static warn(message?:any, ...optionnalParameters:any[]){
        console.warn(message, ...optionnalParameters)
    };

}