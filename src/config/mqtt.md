

# MQTT TOPICS


/example/of/:topic
        :topic //description d'une variable
    "valeur possible 1"
    "valeur possible 2"
    "valeur possible 3"
    "valeur possible 4"


## Malette 

### Base de données

:universId
    0=chambre-sdb
    1=salon-cuisine

* Simuler un click sur d'un des onglets de la base de donnée
/malette/:universId/database/tab/click
    0|1|2 //0:Accueil 1:Recommandations 2:ECommerce


* Simuler run click sur d'un des produit de la partie e-commerce
/malette/:universId/database/product/click
    0|1|2 

* Envoyé lors du click sur le bouton "Acheter" de la partie e-commerce
/malette/:universId/database/product/purchase
    1 14
        //productId:0|1|2,  // Produit choisi
        //activityId:0-16+    // Action en cours
    

* Envoyé lors du click sur du bouton "Action" d'une activité
/malette/:universId/database/action/
    {
        activityId:0-16+  // Action en cours
    }

*Activity complete
/activty/:universId/:activityId/done

* Envoyé par le backend lors du click sur un indicateur de la partie scan
/malette/:universId/activity/set
    {
        "categoryName":"",
        "home":{
            "desc":string, // Text court 
            "action"?:string, // Texte du bouton action // si null ou absent, ne pas afficher
            "actionPlaySound"?:string // url de la mélodie a jouer  // si null ou absent, ne pas afficher
        },
        "database"?:{ // si null ou absent, rendre innactif l'onglet  
            "desc":string, // Text court 
        },
        "ecommerce"?:{ // si null ou absent, rendre innactif l'onglet  
            "products":[
                {
                    "imgSrc":string, // url de l'image
                    "title":string, //Intitulé
                    "desc":string // Text court
                    "energyClass"?:number // 0=A+++ 1=A++ 2=A+ 3=A 4=B 5=C 6=7 // si null ou absent, ne pas afficher
                },
                {
                    "imgSrc":string, // url de l'image
                    "title":string, //Intitulé
                    "desc":string // Text court
                    "energyClass"?:number // 0=A+++ 1=A++ 2=A+ 3=A 4=B 5=C 6=7 // si null ou absent, ne pas afficher
                },
                {
                    "imgSrc":string, // url de l'image
                    "title":string, //Intitulé
                    "desc":string // Text court
                    "energyClass"?:number // 0=A+++ 1=A++ 2=A+ 3=A 4=B 5=C 6=7 // si null ou absent, ne pas afficher
                },
            ]

        }
    }


### Scan

* Envoyé par le front lors du click sur le scan (si le scan est actif)
/malette/:universId/scan/click

* Envoyé par le backend pour activer ou desactiver la possibilité de scanner
/malette/:universId/scan/state
    0|1 // Active ou innactive


* Envoyé par le backend pour afficher un des messages dans le panneau de droite du scan
/malette/:universId/scan/message/:id
        :id = 0|1|2
    string : Message a afficher

* Envoyé par le backend pour changer l'état des pins sur le scan. Remplace les informations précédentes
/malette/:universId/scan/pins/
    [
        {activityId:number, x:number, y:number, color:string},
        {activityId:number, x:number, y:number, color:string},
        {activityId:number, x:number, y:number, color:string},
        ...
    ]

### General

* Envoyé par le backend pour changer l'état global des écrans de la malette
/malette/:universId/display-mode
    "off" // écran noirs
    "locked" 
    "enigme"
    "on"
    "result-win"
    "result-lose"

### Verouillé

* Envoyé par le front-end lors du click sur entrer sur le pavé numérique
/malette/:universId/lock/submit
    string //code transmit


* Envoyé par le backend pour jouer le son :soundId
/malette/:universId/play-sound/
    soundId // id du son 
    
## Thermostats


:thermostatId
    0=chambre-sdb
    1=salon-cuisine

* Envoyé par le front-end lors du click sur validation
/thermostat/:thermostatId/programmation
    100010 ... // Liste des case cochées (1) ou non cochées (0): lundi-plage1 lundi-plage2 ... lundi--plageN mardi-plage1 mardi-plage2 ... mardi--plageN ... dimanche-plage1 dimanche-plage2 ... dimanche--plageN

0:101101101101101101101
101
101
101
101
101
101
101

1:1001100110011001100101110111
1001
1001
1001
1001
1001
1001
0111
0111


/thermostat/:thermostatId/temperature
    23 //temperature du thermostat


## Coffre

:id
    0=chambre-sdb
    1=salon-cuisine

* Envoyé par le backend pour changer l'état global des écrans du coffre
/coffre/:id/mode
    "locked"
    "on"

* Envoyé par le backend pour afficher l'état d'un badge
/coffre/:id/badge/:badgeId
        :badgeId 
    {
        icon:string, //url
        name:string, //text
        active:boolean 
    }

* Envoyé par le backend pour changer ou definir l'horodatage de fin de partie
/coffre/:id/end-timestamp
    number // timestamp de fin


* Envoyé par le backend pour changer ou definir le niveau de la jauge actuel
/coffre/:id/jauge
    number // valeur en kw


* Envoyé par le backend pour lancer la video :videoId sur l'écran
/coffre/:id/play-video/
    videoId // id de la vidéo 

* Envoyé par le backend pour jouer le son :soundId
/coffre/:id/play-sound/
    soundId // id du son 

## PC Chambre

* Envoyé par le backend pour changer l'état global des écrans du PC
/pc-chambre/mode
    "locked"
    "optimisation"
    "co2"
    "avion"
    "veille”

* Envoyé par le front-end pour affirmer la réussite d'un des points
/pc-chambre/success
    "optimisation"
    "co2"
    "avion"
    "veille”


## TV Salon

* Envoyé par le backend pour activer la vidéo
/tv-salon/mode
    "on"
    "off"






###########

# Hardware MICRO

/micro/cafetiere
    ???

/micro/casserole
   ???

/micro/telecommande/out
   touche télécommande (en String) : de "0" à "9","-1" si une autre touche est pressée, "on-off" pour la touche allumage/extinction

/micro/frigo/out
   état de la porte : 1 ouvert, 0 fermé
/micro/frigo/command
    commande de la lampe : 0 éteindre, 1 allumer

/micro/seche-linge/out 
    position bouton rotatif : entre 0 et 15, envoi quand changement de position

/micro/lave-linge/out
    position bouton rotatif : entre 0 et 15, envoi quand changement de position (parfois la position 0 sort un 7)

/micro/lave-vaisselle/out
    0-5 // info au click

/macro/voltage-sensor/out 
    voltage : entre 0.0 et 60.0

/macro/voltage-sensor/command
    temps entre envoi de données en ms (éviter de mettre moins que 50)



MISSING :
    Ampoules


###########

# Hardware MACRO
:pupitreId
    "pupitregauche"
    "pupitredroit"
:boutonId
    "bouton1"
    "bouton2"
    "bouton3"
    "bouton4"
    "bouton5"
    "bouton6"

* Bouton activable ou non
/macro/:pupitreId/encoder/:boutonId/dispo
    1/0

* Activation du bouton
/macro/:pupitreId/encoder/:boutonId/onoff
    1/0

* sens de rotation du bouton
/macro/:pupitreId/encoder/:boutonId/rot
    -1/1

* vitesse du bouton
/macro/:pupitreId/encoder/:boutonId/speed
    0-255

* valeur du bouton
/macro/:pupitreId/encoder/:boutonId/lum
    0-255

* Jauge
/macro/:pupitreId/jauge/:boutonId/
    0-5

* picto
/macro/:pupitreId/scan/
    0-27


