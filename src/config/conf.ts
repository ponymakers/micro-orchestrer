import Universe from '../vo/Universe';


export default class conf{


    //================================================================================
    // MQTT
    //================================================================================
    public static get mqttBrokerUrl():string{
        //return process.env.MQTT_BROKER_URL || 'wss://test.mosquitto.org:8081';
        //return process.env.MQTT_BROKER_URL || 'ws://localhost:9001';
        //return process.env.MQTT_BROKER_URL || 'http://192.168.1.44:1884'; // MACRO
        return process.env.MQTT_BROKER_URL || 'http://192.168.1.43:1883'; // MICRO

    }


    //================================================================================
    // MQTT Topics
    //================================================================================
    public static localTest = false;

    public static mqttTopic_DatabaseTabClick(universeId:Universe|string=":universeId"):string{
        return '/malette/'+universeId+'/database/tab/click';
    }

    public static mqttTopic_DatabaseProductClick(universeId:Universe|string=":universeId"):string{
        return '/malette/'+universeId+'/database/product/click';
    }

    public static mqttTopic_DatabaseProductPurchase(universeId:Universe|string=":universeId"):string{
        return '/malette/'+universeId+'/database/product/purchase';
    }

    public static mqttTopic_ActivitySet(universeId:Universe|string=":universeId"):string{
        return '/malette/'+universeId+'/product/category/set';
    }

    public static mqttTopic_ScanSend(universeId:Universe|string=":universeId"):string{
        return '/malette/'+universeId+'/scan/click';
    }

    public static mqttTopic_ScanActivationState(universeId:Universe|string=":universeId"):string{
        return '/malette/'+universeId+'/scan/state';
    }

    public static mqttTopic_ScanMessage(universeId:Universe|string=":universeId", id:string=":id"):string{
        return '/malette/'+universeId+'/scan/message/'+id;
    }

    public static mqttTopic_ScanPin(universeId:Universe|string=":universeId"):string{
        return '/malette/'+universeId+'/lock/set';
    }

    public static mqttTopic_LockPadSubmit(universeId:Universe|string=":universeId"):string{
        return '/malette/'+universeId+'/lock/submit';
    }

    public static mqttTopic_LockSet(universeId:Universe|string=":universeId", pin:string=":pin"):string{
        return '/malette/'+universeId+'/scan/pin/'+pin;
    }


    //static 
    static mqttTopic_GamePhase(): string {
        return '/game/phase/';
    }
    static mqttTopic_JaugeValue(universeId:Universe|string=":universeId"): string {
        return '/coffre/'+universeId+'/jauge';
    }
    static mqttTopic_GameTimestamp(): string {
        return '/game/end-timestamp';
    }
    static mqttTopic_PhaseTimestamp(universeId:Universe|string=":universeId"): string {
        return '/game/'+universeId+'/end-timestamp';
    }
    // * Envoyé par le backend pour changer l'état global des écrans du PC
    // pc-chambre/mode
    //     "locked"
    //     "optimisation"
    //     "co2"
    //     "avion"
    //     "veille

    public static get mqttTopic_PC_mode():string{
        return '/pc-chambre/mode';
    }


    // * Envoyé par le front-end pour affirmer la réussite d'un des points
    // pc-chambre/success
    //     "optimisation"
    //     "co2"
    //     "avion"
    //     "veille
    
    public static get mqttTopic_PC_success():string{
        return '/pc-chambre/success';
    }


    public static mqttTopic_ActionComplete(universId:string|Universe=":universId", activityId=":activityId"): string {
        return "activty/"+universId+"/"+activityId+"/success";
    }

    public static mqttTopic_ActionFailled(universId:string|Universe=":universId", activityId=":activityId"): string {
        return "activty/"+universId+"/"+activityId+"/failed";
    }

    public static mqttTopic_ActionsStates(universId:string|Universe=":universId"): string {
        return "activties/"+universId+"/states";
    }


    //================================================================================
    // Audio
    //================================================================================

    static mqttTopic_AudioRoom(universeId:Universe|string=":universeId"): string {
        return '/sound/room/'+universeId;
    }

    static mqttTopic_AudioMalette(universeId:Universe|string=":universeId"): string {
        return '/sound/malette/'+universeId;
    }

    // # Hardware MACRO
    // :pupitreId
    //     "pupitregauche"
    //     "pupitredroit"
    // :boutonId
    //     "bouton1"
    //     "bouton2"
    //     "bouton3"
    //     "bouton4"
    //     "bouton5"
    //     "bouton6"

    // * ???
    // /macro/:pupitreId/encoder/:boutonId/dispo
    //     ???
    public static mqttTopic_Pupitre_Encoder_Dispo(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/dispo';
    }

    // * Activation du bouton
    // /macro/:pupitreId/encoder/:boutonId/onoff
    //     1/0
    public static mqttTopic_Pupitre_Encoder_OnOff(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/onoff';
    }

    // * sens de rotation du bouton
    // /macro/:pupitreId/encoder/:boutonId/rot
    //     -1/1
    public static mqttTopic_Pupitre_Encoder_Rot(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/rot';
    }

    // * vitesse du bouton
    // /macro/:pupitreId/encoder/:boutonId/speed
    //     0-255
    public static mqttTopic_Pupitre_Encoder_Speed(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/speed';
    }

    // * valeur du bouton
    // /macro/:pupitreId/encoder/:boutonId/lum
    //     0-255
    public static mqttTopic_Pupitre_Encoder_Lum(pupitreId:string):string{
        return '/macro/'+pupitreId+'/encoder/:boutonId/lum';
    }

    // * Jauge
    // /macro/:pupitreId/jauge/:boutonId/
    //     0-5
    public static mqttTopic_Pupitre_Jauge(pupitreId:string):string{
        return '/macro/'+pupitreId+'/jauge/:boutonId';
    }

    // * picto
    // /macro/:pupitreId/scan/
    //     0-27
    public static mqttTopic_Pupitre_Scan(pupitreId:string):string{
        return '/macro/'+pupitreId+'/scan';
    }



    //================================================================================
    // Salon
    //================================================================================
    // * Display television video
    // * 'on' / 'off'
    public static get mqttTopic_salon_tv_mode():string{
        return '/tv-salon/mode';
    }

    static mqttTopic_GameState(): string {
        return "/admin/game/state";
    }


    //================================================================================
    // Content
    //================================================================================
    public static get database_content():any{
        return [{
                    "productCategory":"Réfrigérateur",
                    "home":{
                        "html":"Ex Lorem laborum eiusmod duis. Enim officia voluptate adipisicing deserunt nostrud. Sit do sit nostrud non aliquip deserunt. Sunt nulla reprehenderit anim irure incididunt irure nulla. In quis in nisi amet dolore dolore amet ullamco aute dolor esse. Amet tempor eiusmod excepteur est ullamco adipisicing."
                    },
                    "database":{
                        "html":"In sunt qui veniam duis quis eu irure sit est sit eu laborum. Reprehenderit adipisicing consequat non do eu Lorem nostrud nostrud consequat et ea. Ullamco commodo deserunt eiusmod ex ea. Est anim anim labore ut qui dolor quis. Laborum voluptate aliquip dolore nisi dolore. Nisi voluptate nulla ad do ea duis in ullamco labore tempor sit quis aliqua sit. Ex id sint irure voluptate ad exercitation quis adipisicing cupidatat culpa tempor cillum."
                    },
                    "ecommerce":{
                        "products":[
                            {
                                "imgSrc":"https://www.economax.com/wcsstore/BMCatalogAssetStore/images/main/00377181_10_FRONT.png",
                                "canPurchasse":true,
                                "title":"Frigo 1",
                                "description":"Commodo occaecat voluptate id tempor duis in sunt consequat reprehenderit quis ut quis proident. Elit cupidatat nisi aliqua aliqua enim Lorem fugiat nulla anim minim non eu. Proident officia mollit velit commodo nostrud cupidatat reprehenderit in pariatur tempor reprehenderit magna aute enim. Ut excepteur aute tempor sunt veniam eu aliqua laborum. Amet ullamco amet quis proident Lorem Lorem commodo dolor consectetur tempor."
                            },
                            {
                                "imgSrc":"https://www.economax.com/wcsstore/BMCatalogAssetStore/images/main/00373330_10_FRONT.png",
                                "canPurchasse":true,
                                "title":"Frigo 2",
                                "description":"Fugiat ut nisi mollit ex dolor non elit nisi quis ex excepteur nisi aute commodo. Excepteur fugiat labore occaecat ad fugiat voluptate tempor aliquip do laboris id non. Dolore enim commodo do laboris Lorem sunt proident proident nostrud eu est elit magna non. Nostrud proident amet cillum tempor occaecat dolor adipisicing. Aliqua exercitation cupidatat occaecat duis."
                            },
                            {
                                "imgSrc":"https://www.frigidaire.ca/Content/Images/WaterFilterFinderTool/water_filter_side_by_side.png",
                                "canPurchasse":true,
                                "title":"Frigo 3",
                                "description":"Eiusmod et culpa Lorem amet velit dolore enim et adipisicing. Ad sit velit aute culpa tempor. Et nostrud aliqua sit laborum eiusmod dolore amet."
                            },

                        ]

                    }
                },
                {
                    "productCategory":"Isolation",
                    "home":{
                        "html":"Reprehenderit adipisicing reprehenderit anim labore culpa magna commodo. Amet irure id esse aliquip anim mollit laboris id Lorem ipsum nisi mollit ullamco ad. Dolore eiusmod proident incididunt aliqua laborum sint commodo et deserunt culpa irure quis laboris occaecat. Adipisicing deserunt aliquip consectetur eu nostrud pariatur pariatur ullamco dolor nisi exercitation. Ad enim exercitation sit ipsum aliqua laborum commodo tempor laborum esse esse dolore quis sit."
                    },
                    "database":{
                        "html":"Dolore non cillum occaecat sit labore eiusmod aliqua ut culpa. Fugiat ullamco sint cillum exercitation fugiat officia ipsum. Cillum aute elit irure do nostrud veniam reprehenderit."
                    },
                    "ecommerce":{
                        "products":[
                            {
                                "imgSrc":"http://www.st-isolation.com/_media/img/small/photo-accueil-2.png",
                                "canPurchasse":true,
                                "title":"Frigo 1",
                                "description":"Dolore mollit dolore dolore laborum. Exercitation velit aliquip ea incididunt labore quis sunt consequat in nostrud officia. Minim tempor incididunt Lorem elit qui labore. Ad exercitation ut mollit et nulla consequat aute ut tempor exercitation incididunt. Dolore non sit labore deserunt commodo voluptate enim veniam. Lorem ex sint anim deserunt irure labore proident."
                            },
                            {
                                "imgSrc":"https://www.coeurdefoyer.fr/1845-large_default/quart-de-panneau-de-vermiculite.jpg",
                                "canPurchasse":true,
                                "title":"Frigo 2",
                                "description":"Veniam velit cupidatat adipisicing velit excepteur in id ad pariatur esse qui mollit. Occaecat laborum amet eu aute consequat cillum est nostrud aliqua veniam esse esse eu. Consectetur adipisicing Lorem ipsum sunt aute est in quis. Culpa consectetur officia aute id ullamco cillum dolore esse aute laboris magna id et sunt. Aliqua et elit consectetur Lorem aliquip aliquip commodo fugiat aute aliqua pariatur incididunt est ipsum."
                            },
                            {
                                "imgSrc":"http://www.briqueiso.com/img/parallax-slider/images/1.png",
                                "canPurchasse":true,
                                "title":"Frigo 3",
                                "description":"Commodo Lorem voluptate sunt officia sint id enim nulla magna et et consequat ea. Nostrud nostrud eiusmod consectetur quis in anim sunt irure voluptate. Irure est esse incididunt ullamco do reprehenderit qui laborum aute tempor est veniam. Ex eu duis do eiusmod eu ullamco ad anim labore et voluptate et. Ea aliquip culpa et eiusmod non ex commodo."
                            },

                        ]

                    }
                }
            ]
        }


}
