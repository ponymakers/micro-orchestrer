import GameEngine from "./game/GameEngine";
import PubSub from './controllers/PubSub';

PubSub.init();

GameEngine.init();