import GameState, { ActionState } from '../vo/GameState';
import Universe from '../vo/Universe';
import Logger from '../utils/Logger';
import PubSub, { MqttEvent } from '../controllers/PubSub';
import conf from '../config/conf';
import { ActionSource } from '../vo/ActionSource';
import db from '../db/universesDB';
import universesDB from '../db/universesDB';
import ActionRef from '../vo/ActionRef';
import ScreenStates from '../vo/ScreenStates';


const UNIVERSE_PHASE_COMEBACK = -1;

var sendPinDelay = 3000;
var gameDuration = 45 * 60 * 1000;

const universesIds = [Universe.CHAMBRE_SDB, Universe.SALON_CUISINE]

class GameEngine {

    gameState:GameState ;
    screenStates:ScreenStates = new ScreenStates();

    public constructor(){
        this.gameState = this.gameStateInit();
        // this.sendActionRefs();
    }

    
    gameStateInit():GameState{
        return {
            gameEndTimeStamp:0, 
            phase:"IDLE", 
            universes:universesIds.map((universeId)=>{
                const universeDB = universesDB[universeId];
                return {
                    currentPhase: 0, 
                    currentPhaseEndTimeStamp:0,
                    done:false,
                    actions:universeDB.map(actionRef=>{
                        return {
                            definition:actionRef,
                            state:{
                                done:false,
                                sequence:"",
                                success:false,
                            }
                        }
                    })
                }
            })
        } 
    }
    
    init(){
        // PubSub.sub(  "/micro/admin/game/start", (ev)=>this.doStartGame() )
        // PubSub.sub(  "/micro/admin/game/stop", (ev)=>this.doEndGame() );
        // PubSub.sub(  "/micro/admin/universe/:universeId/start", (ev)=>this.doStartUniverse( parseInt(ev.props.universeId) )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/end", (ev)=>this.doEndUniverse( parseInt(ev.props.universeId) )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/phase/end", (ev)=>this.doEndPhase( parseInt(ev.props.universeId) )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/phase/start", (ev)=>this.doStartPhase( parseInt(ev.props.universeId), parseInt(ev.props.phaseId) )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/action/validate", (ev)=>this.doValidateAction( parseInt(ev.props.universeId), ev.props.actionId )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/action/fail", (ev)=>this.doFailAction( parseInt(ev.props.universeId), ev.props.actionId )  );
        PubSub.subAll((ev)=>this.onMessageReceived(ev) );
        
        PubSub.connected(()=>{
            this.sendGameState();
        })

        this.gameState = this.gameStateInit();

        setInterval(()=>this.onTick(), 1000)
    }


    onMessageReceived(ev:MqttEvent){
        if(ev.topic.startsWith("/admin/")){

        }
        else if(ev.topic.startsWith("/micro/admin/")){                    
            let sub = ev.topic.split("/");

            try{

                if(ev.topic.startsWith("/micro/admin/session/")){

                    switch(sub[4]){
                        case "reset" : 
                            this.doResetSession();
                        break;
                        // case "stop" : 
                        //     this.doEndGame();
                        // break;
                    }
                }
                if(ev.topic.startsWith("/micro/admin/game/")){

                    switch(sub[4]){
                        case "start" : 
                            this.doStartGame();
                        break;
                        case "stop" : 
                            this.doEndGame();
                        break;
                    }
                }
                if(ev.topic.startsWith("/micro/admin/universe/")){
                    let universeId = parseInt(sub[4]) as Universe;
                    switch(sub[5]){
                        case "start":
                            this.doStartUniverse(universeId)
                            break;
                        case "end":
                            this.doEndUniverse(universeId)
                            break;
                        case "phase":
                            switch(sub[6]){
                                case "start" : 
                                    this.doStartPhase(universeId, parseInt(ev.payload));
                                break;
                                case "end" : 
                                    this.doEndPhase(universeId);
                                break;
                            }
                            break;
                        case "action":
                            switch(sub[6]){
                                case "validate" : 
                                    this.doValidateAction(universeId, ev.payload);
                                break;
                                case "fail" : 
                                    this.doFailAction(universeId, ev.payload);
                                break;
                            }
                            break;
                    }
                }
                    
            }catch(ex){
                console.error("failled to read admin topic due to : ", ex)
            }
        }else if(ev.topic === conf.mqttTopic_ScanSend(0)){
            this.onScanRequest(0);
        }else if(ev.topic === conf.mqttTopic_ScanSend(1)){
            this.onScanRequest(1);
            
        }
        else{
            this.testActions(ev);
        }
    }

    testActions(ev: MqttEvent) {

        if(ev.topic.startsWith("/micro/coffre/")){
            this.onCardScanEvent(ev);
            return;
        }

        for (const universe of universesIds) {
            let state = this.universeState(universe);
            let actions = this.getPhaseActions(universe, state.currentPhase)
            //console.log("testActions", universe, JSON.stringify(actions))

            for (const action of actions) {
                if(action.state.done){
                    continue;
                }
                let definition = action.definition;
                let validationTopic= definition.validationTopic;
                let validationRequestedValue  = definition.validationRequestedValue;
                
                switch(action.definition.validationType){
                    case "SIGNAL":{
                        console.debug("SIGNAL", '['+validationTopic+'=='+ev.topic+']', "value", '['+validationRequestedValue+'=='+ev.payload.trim()+']')
                        if( validationTopic == ev.topic ){
                            if(!validationRequestedValue
                                || validationRequestedValue == ev.payload.trim()){
                                this.doValidateAction(universe, action.definition.id);
                            }
                        }
                        break;
                    }
                    case "SEQUENCE":{

                        if( validationTopic == ev.topic && validationRequestedValue){
                            action.state.sequence+=ev.payload.trim()
                            console.debug("SEQUENCE", '['+validationTopic+'=='+ev.topic+']', "value", '['+validationRequestedValue+'=='+ev.payload.trim()+']', action.state.sequence)

                            if(action.state.sequence.endsWith(validationRequestedValue)){
                                this.doValidateAction(universe, action.definition.id);
                            }else{
                                if(action.state.sequence.length > validationRequestedValue.length){
                                    action.state.sequence = action.state.sequence.substring(-validationRequestedValue.length);
                                }
                            }
                        }

                        break;
                    }
                    case "CUSTOM":{
                        //NOPE
                        break;
                    }
                }
            }
        }
    }

    onCardScanEvent(ev: MqttEvent) {
        let universe = 1-parseInt(ev.topic.split("/")[3]);

        let state = this.universeState(universe);
        let currentScanActions 
            = this.getPhaseActions(universe, state.currentPhase).filter(a=>a.definition.validationType == "SCAN")

        console.log("currentScanActions", currentScanActions);
        let correct = false;
        let badgeId = ev.payload.slice(-8,-2);
        console.log("badgeId ["+badgeId+"]");

        for(let action of currentScanActions){
            if(action.definition.validationRequestedValue 
                && action.definition.validationRequestedValue.indexOf(badgeId) != -1){
                    correct = true;
                    this.doValidateAction(universe, action.definition.id)
                }
        }

        if(!correct){
            this.muteScan(universe);
        }
    }

    muteScan(universe:Universe){
        if(this.screenStates.getCoffre(universe)){
            this.screenStates.setCoffre(universe, (state)=>{return {...state, scanActive:false, scanInactiveTimestamp:Date.now()+30*1000}})
        }
    }

    /////////////////////
    // GENERAL

    
    reset(){
        this.init();
    }

    doResetSession(){
        console.debug("doResetSession")
        this.gameState = this.gameStateInit();
        PubSub.pub(conf.mqttTopic_salon_tv_mode, "on");
        // TODO Open coffres
        // TODO veille thermostat

        this.sendGameState();
    }

    doStartGame(){
        console.debug("doStartGame")
        this.gameState.gameEndTimeStamp = Date.now() + gameDuration;
        this.gameState.phase = "GAME";
        this.sendGameState();
        this.onStartGame();
    }

    doEndGame(){
        console.debug("doEndGame")
        this.gameState.phase = "END";
        this.sendGameState();
        this.onEndGame();
    }

    checkGameState(){
        if(this.gameState.phase=="GAME" && this.isGameSuccessful()){
            this.doEndGame();
        }
    }

    onStartGame(){
        this.sendGameTimestamp(this.gameState.gameEndTimeStamp);
        this.sendGamePhase(this.gameState.phase);
    }


    onEndGame(){
        // WOWOWOWOWOW Trop de truc. A verifier avec tout le monde
        this.sendGamePhase("end");
        this.sendGameState();
    }

    onTick(){
        let now = Date.now();
        
        if(this.isGameRunning()){

            for (const universeId of universesIds) {
                let state = this.universeState(universeId)
                if(state.currentPhaseEndTimeStamp && now > state.currentPhaseEndTimeStamp){
                    this.doEndPhase(universeId);
                }
            }

            if(now > this.gameState.gameEndTimeStamp){
                this.doEndGame();
            }
        }

        for (const universeId of universesIds) {
            let coffreState = this.screenStates.getCoffre(universeId)
            if( (!coffreState.scanActive) && now > coffreState.timestamp){
                this.screenStates.setCoffre(universeId, (state)=>{return {...state, scanActive:true}});
            }
        }
    }


    isGameRunning():boolean{
        switch(this.gameState.phase){  
            case "GAME":
                return true;
            case "IDLE":
            case "END":
            case "INTRO":
            default :
                return false;
        }
    }

    universeState(universe:Universe){
        return this.gameState.universes[universe];
    }


    isGameSuccessful(){
        let success = true;
        for (const universeId of universesIds) {
            let universe = this.universeState(universeId)
            if(universe.done == false){
                success = false;
            }
        }
        return success;
    }

    /////////////////////
    // UNIVERSE

    doStartUniverse(universe:Universe) {
        console.debug("doStartUniverse", universe)
        this.doStartPhase(universe, 1);
        let universeState = this.universeState(universe)
        universeState.done = false;
        console.debug("  universeState", universeState)
        this.sendGameState();
    }


    doEndUniverse(universe:Universe) {
        console.debug("doEndUniverse", universe)
        this.universeState(universe).done = true;
        this.checkGameState();
        this.sendGameState();
    }

    /////////////////////
    // PHASES


    doStartPhase(universe:Universe, phaseId:number){
        console.debug("doStartPhase", universe, phaseId)

        let universeState = this.universeState(universe)
        if(universeState.currentPhase != phaseId){
            universeState.currentPhase = phaseId;
            let phaseDuration = this.getPhaseDuration(universe, phaseId);
            let phaseEndTimestamp = Date.now() + phaseDuration*1000*60;    
            universeState.currentPhaseEndTimeStamp = phaseEndTimestamp;

            this.onStartPhase(universe, phaseId);
            for(const action of this.getPhaseActions(universe, phaseId)){
                this.onActionEnabled(universe, action);
            }
        }
        this.sendGameState();
    }
    onActionEnabled(universe: Universe, action: ActionState) {
        if(action.definition.onStartTopic){
            PubSub.pub(action.definition.onStartTopic, action.definition.onStartMessage)
        }
    }

    doEndPhase(universe: Universe) {
        console.debug("doEndPhase")
        let universeState =  this.universeState(universe);
        let lastPhase = this.getPhaseCount(universe);
        this.onEndPhase(universe, universeState.currentPhase);
        if( universeState.currentPhase == lastPhase ){
            if(this.areActionDone(universeState.actions)){
                this.doEndUniverse(universe);
            }else{
                this.doStartPhase(universe, UNIVERSE_PHASE_COMEBACK);
            }
        }
        else if(universeState.currentPhase == UNIVERSE_PHASE_COMEBACK){
            if(this.areActionDone(universeState.actions)){
                this.doEndUniverse(universe);
            }
        }else{
            this.doStartPhase(universe, this.universeState(universe).currentPhase + 1);
        }
        this.sendGameState();
    }

    checkPhase(universe:Universe){
        let universeState =  this.universeState(universe);
        let phaseId = universeState.currentPhase;
        let isPhaseDone = this.isPhaseDone(universe, phaseId);
        
        Logger.debug("checkPhase", universe, phaseId, isPhaseDone);

        if(isPhaseDone){
            this.doEndPhase(universe);
        }
    }

    getPhaseCount(universe:Universe):number {
        let phases = this.universeState(universe).actions.map(action=>action.definition.phaseId);
        return Math.max(...phases);
    }

    getPhaseActions(universe:Universe, phaseId:number):ActionState[]{
        if(phaseId==UNIVERSE_PHASE_COMEBACK){
            return this.universeState(universe).actions;
        }
        else{
            return this.universeState(universe).actions.filter(action=>action.definition.phaseId == phaseId)
        }
    }
    
    isPhaseDone(universe:Universe, phaseId:number):boolean{
        let actions = this.getPhaseActions(universe, phaseId);
        if(phaseId != UNIVERSE_PHASE_COMEBACK){
            return this.areActionDone(actions);
        }else{
            return this.areActionSuccessful(actions);
        }
    }

    areActionDone(actions:ActionState[]):boolean{
        for (const action of actions) {
            if(!action.state.done){
                return false;
            }
        }
        return true;
    }
    

    areActionSuccessful(actions:ActionState[]):boolean{
        for (const action of actions) {
            if(!action.state.success){
                return false;
            }
        }
        return true;
    }

    getPhaseDuration(universe:Universe, phaseId:number):number{
        let actions = this.getPhaseActions(universe, phaseId);
        if(actions){
            return actions[0].definition.phaseDuration;
        }
        return -1;
    }

    onStartPhase(universe:Universe, phaseId:number){
        this.sendScanMode(universe, true);
        this.sendPhaseTimestamp(universe, this.gameState.universes[universe].currentPhaseEndTimeStamp || this.gameState.gameEndTimeStamp);
    }

    onEndPhase(universe:Universe, phaseId:number){
        this.sendActiveActions(universe, []);
    }




    /////////////////////
    // ACTIONS



    doValidateAction(universe: Universe, actionId:string) {

        console.debug("doValidateAction")
        let action = this.getAction(universe, actionId);
        if(!action){
            Logger.error("validateAction : action " + actionId + " not found in universe " + universe);
            return;
        }
        action.state.done = true;
        action.state.success = true;
        this.onActionComplete(universe, actionId);
        this.sendGameState();
    }

    doFailAction(universe: Universe, actionId:string) {
        console.debug("doFailAction")
        let action = this.getAction(universe, actionId);
        if(!action){
            Logger.error("validateAction : action " + actionId + " not found in universe " + universe);
            return;
        }
        action.state.done = true;
        action.state.success = false;
        this.onActionFailled(universe, actionId);
        this.sendGameState();
    }

    getAction(universe: Universe, actionId:string){
        let universeState =  this.universeState(universe);
        let action = universeState.actions.find(a=>a.definition.id == actionId);
        return action;
    }

    jaugeCurrentValue(universe: Universe): number {
        let actions = this.universeState(universe).actions.filter(action=>!action.state.done)
        return actions.reduce((p,c)=>{return p + c.definition.value}, 0 )
    }

    jaugeTotalValue(universe: Universe): number {
        let actions = this.universeState(universe).actions;
        return actions.reduce((p,c)=>{return p + c.definition.value}, 0 )
    }

    onScanRequest(universe:Universe){
        this.sendScanMode(universe, false);
        let state = this.universeState(universe);
        setTimeout(()=>{
            let currentActions = this.getPhaseActions(universe, state.currentPhase);
            this.sendActiveActions(universe, currentActions);
            console.log("currentActions", currentActions)
        }, sendPinDelay);
    }

    onActionComplete(universe:Universe, actionId:string){
        let action = this.getAction(universe, actionId);
        this.sendActionComplete(universe, actionId)
        this.sendUpdateJauge(universe, this.jaugeCurrentValue(universe), this.jaugeTotalValue(universe));
        if(action){
            this.sendRoomSound(universe, action.definition.validationSound);
        }
        this.checkPhase(universe);
        if(action && action.definition.onEndTopic){
            PubSub.pub(action.definition.onEndTopic, action.definition.onEndMessage)
        }
    }

    onActionFailled(universe:Universe, actionId:string){
        this.sendActionFailled(universe, actionId);
        this.sendUpdateJauge(universe, this.jaugeCurrentValue(universe), this.jaugeTotalValue(universe));
        this.checkPhase(universe);
    }




    //////////////////
    // Messagers

    sendActiveActions(universe: Universe, actions:ActionState[]): void {
        PubSub.pub(conf.mqttTopic_ScanPin(universe), JSON.stringify(actions) );
    }

    sendScanMode(universe: Universe, on:boolean) {
        PubSub.pub(conf.mqttTopic_ScanActivationState(universe), on ? "1":"0")
    }
    
    sendActionComplete(universe: Universe, actionId: string) {
        PubSub.pub(conf.mqttTopic_ActionComplete(universe, actionId))
    }

    sendActionFailled(universe: Universe, actionId: string) {
        PubSub.pub(conf.mqttTopic_ActionFailled(universe, actionId))
    }

    sendPhaseTimestamp(universe: Universe, timestamp:number) {
        PubSub.pub(conf.mqttTopic_PhaseTimestamp(universe), timestamp.toString())
    }
    
    sendGameTimestamp( timestamp:number) {
        PubSub.pub(conf.mqttTopic_GameTimestamp(), timestamp.toString())
    }

    sendUpdateJauge(universe: Universe, jaugeValue:number, jaugeTotal:number) {
        PubSub.pub(conf.mqttTopic_JaugeValue(universe), [jaugeValue, jaugeTotal].join(" "))
    }

    sendGamePhase(gamePhase:string){
        PubSub.pub(conf.mqttTopic_GamePhase(), gamePhase)
    }

    sendGameState(){
        PubSub.pub(conf.mqttTopic_GameState(), JSON.stringify(this.gameState))
    }
    sendRoomSound(universe: Universe, validationSound: string) {
        PubSub.pub(conf.mqttTopic_AudioRoom(universe), validationSound)

    }
}


export default new GameEngine();

