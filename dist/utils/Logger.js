"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Logger {
    static debug(message, ...optionnalParameters) {
        console.log(message, optionnalParameters);
    }
    ;
    static log(message, ...optionnalParameters) {
        console.log(message, ...optionnalParameters);
    }
    ;
    static error(message, ...optionnalParameters) {
        console.error(message, ...optionnalParameters);
    }
    ;
    static warn(message, ...optionnalParameters) {
        console.warn(message, ...optionnalParameters);
    }
    ;
}
exports.default = Logger;
//# sourceMappingURL=Logger.js.map