"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const dbActionsChambre_1 = tslib_1.__importDefault(require("./dbActionsChambre"));
const dbActionsSalon_1 = tslib_1.__importDefault(require("./dbActionsSalon"));
const util_1 = require("util");
const Universe_1 = tslib_1.__importDefault(require("../vo/Universe"));
let db = {
    chambre: dbActionsChambre_1.default,
    salon: dbActionsSalon_1.default
};
function actionSourceToActionRef(source) {
    return {
        name: source.name,
        id: source.id,
        subId: source.subId,
        phaseId: source.phaseId,
        phaseDuration: source.phaseDuration,
        homeText: source.actionTexte || source.recoTexte || source.remplacementTexte || source.diversTexte || "",
        value: source.manipulationValue || source.recoValue || source.remplacementValue || source.divers || 0,
        actionLabel: source.actionLabel,
        validationTopic: source.validationTopic,
        validationRequestedValue: source.validationRequestedValue,
        validationType: source.validationType,
        onStartTopic: source.onStartTopic,
        onStartMessage: source.onStartMessage,
        onEndTopic: source.onEndTopic,
        onEndMessage: source.onEndMessage,
        yesNo: util_1.isUndefined(source.yesNo) ? false : source.yesNo,
        bddReco: source.bddReco,
        cardRef: source.cardref,
        validationSound: source.validationSound,
    };
}
const universesDB = [];
universesDB[Universe_1.default.CHAMBRE_SDB] = dbActionsChambre_1.default.map(actionSourceToActionRef);
universesDB[Universe_1.default.SALON_CUISINE] = dbActionsSalon_1.default.map(actionSourceToActionRef);
exports.default = universesDB;
//# sourceMappingURL=universesDB.js.map