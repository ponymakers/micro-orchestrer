import dbChambre from "./dbActionsChambre";
import dbSalon from "./dbActionsSalon";
import { isUndefined } from 'util';
import Universe from '../game/Universe';
let db = {
    chambre: dbChambre,
    salon: dbSalon
};
function actionSourceToActionRef(source) {
    return {
        name: source.name,
        id: source.id,
        subId: source.subId,
        phaseId: source.phaseId,
        phaseDuration: source.phaseDuration,
        homeText: source.actionTexte || source.recoTexte || source.remplacementTexte || source.diversTexte || "",
        value: source.manipulationValue || source.recoValue || source.remplacementValue || source.divers || 0,
        actionLabel: source.actionLabel,
        validationTopic: source.validationTopic,
        validationRequestedValue: source.validationRequestedValue,
        validationType: source.validationType,
        onStartTopic: source.onStartTopic,
        onStartMessage: source.onStartMessage,
        onEndTopic: source.onEndTopic,
        onEndMessage: source.onEndMessage,
        yesNo: isUndefined(source.yesNo) ? false : source.yesNo,
        bddReco: source.bddReco,
        cardRef: source.cardref,
        validationSound: source.validationSound,
    };
}
db[Universe.CHAMBRE_SDB] = dbChambre.map(actionSourceToActionRef);
db[Universe.SALON_CUISINE] = dbSalon.map(actionSourceToActionRef);
export default db;
//# sourceMappingURL=base.js.map