"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mqtt = tslib_1.__importStar(require("mqtt"));
const conf_1 = tslib_1.__importDefault(require("../config/conf"));
const Logger_1 = tslib_1.__importDefault(require("../utils/Logger"));
class PubSub {
    constructor() {
        this._handlers = [];
        this._handlersAll = [];
        this._handlerConnected = [];
    }
    get client() {
        return this._client ? this._client : this.init();
    }
    init() {
        this._client = mqtt.connect(conf_1.default.mqttBrokerUrl);
        this._client.on("connect", (status) => {
            console.log("MQTT connected");
            for (let handler of this._handlerConnected) {
                handler();
            }
        });
        this._client.on("message", (topic, message) => {
            this.handleMessage(topic, message);
        });
        return this._client;
    }
    ;
    connected(handler) {
        this._handlerConnected.push(handler);
    }
    handleMessage(topic, message) {
        let handled = false;
        Logger_1.default.log("MQTT received : ", topic, "with message", message.toString().substring(0, 30));
        let event = {
            topic,
            payload: message.toString(),
            props: {},
            listenerTopic: ""
        };
        for (const handler of this._handlersAll) {
            console.log(this._handlersAll);
            event.listenerTopic = "#";
            handler(event);
        }
        for (const handler of this._handlers) {
            let messageSubTopics = topic.split("/");
            let handlerSubTopics = handler.topic.split("/");
            if (handlerSubTopics.length != messageSubTopics.length) {
                continue;
            }
            let match = true;
            let props = {};
            for (let i = 0; i < handlerSubTopics.length; i++) {
                let messageSubTopic = messageSubTopics[i];
                let handlerSubTopic = handlerSubTopics[i];
                if (handlerSubTopic.startsWith(":")) {
                    props[handlerSubTopic.substr(1)] = messageSubTopic;
                    continue;
                }
                if (messageSubTopic == handlerSubTopic) {
                    continue;
                }
                match = false;
                break;
            }
            if (match) {
                event.props = props;
                event.listenerTopic = handler.topic;
                handler.handler(event);
                handled = true;
            }
        }
        if (!handled) {
            //Logger.warn("MQTT Unhandled topic", topic, "with message", message.toString())
        }
    }
    sub(topic, handler) {
        this._handlers.push({ topic, handler });
        if (this._client) {
            let formattedTopic = topic.split("/")
                .map((subTopic) => {
                return subTopic.startsWith(":") ? "+" : subTopic;
            }).join("/");
            Logger_1.default.log("MQTT sub : ", topic, ":", "[" + formattedTopic + "]");
            this._client.subscribe(formattedTopic);
        }
    }
    subAll(handler) {
        this._handlersAll.push(handler);
        if (this._client) {
            this._client.subscribe("#");
        }
    }
    pub(topic, payload = "") {
        if (this._client) {
            Logger_1.default.log("MQTT publish : ", topic, ":", "[" + payload.substring(0, 30) + "]");
            this._client.publish(topic, payload || "");
        }
        else {
            Logger_1.default.warn("MQTT Message not send : ", topic, "with message", payload);
        }
    }
}
exports.default = new PubSub();
//# sourceMappingURL=PubSub.js.map