"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Universe_1 = tslib_1.__importDefault(require("../vo/Universe"));
const Logger_1 = tslib_1.__importDefault(require("../utils/Logger"));
const PubSub_1 = tslib_1.__importDefault(require("../controllers/PubSub"));
const conf_1 = tslib_1.__importDefault(require("../config/conf"));
const universesDB_1 = tslib_1.__importDefault(require("../db/universesDB"));
const ScreenStates_1 = tslib_1.__importDefault(require("../vo/ScreenStates"));
const UNIVERSE_PHASE_COMEBACK = -1;
var sendPinDelay = 5000;
var gameDuration = 45 * 60 * 1000;
const universesIds = [Universe_1.default.CHAMBRE_SDB, Universe_1.default.SALON_CUISINE];
class GameEngine {
    constructor() {
        this.screenStates = new ScreenStates_1.default();
        this.gameState = this.gameStateInit();
        // this.sendActionRefs();
    }
    gameStateInit() {
        return {
            gameEndTimeStamp: 0,
            phase: "IDLE",
            universes: universesIds.map((universeId) => {
                const universeDB = universesDB_1.default[universeId];
                return {
                    currentPhase: 0,
                    currentPhaseEndTimeStamp: 0,
                    done: false,
                    actions: universeDB.map(actionRef => {
                        return {
                            definition: actionRef,
                            state: {
                                done: false,
                                sequence: "",
                                success: false,
                            }
                        };
                    })
                };
            })
        };
    }
    init() {
        // PubSub.sub(  "/micro/admin/game/start", (ev)=>this.doStartGame() )
        // PubSub.sub(  "/micro/admin/game/stop", (ev)=>this.doEndGame() );
        // PubSub.sub(  "/micro/admin/universe/:universeId/start", (ev)=>this.doStartUniverse( parseInt(ev.props.universeId) )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/end", (ev)=>this.doEndUniverse( parseInt(ev.props.universeId) )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/phase/end", (ev)=>this.doEndPhase( parseInt(ev.props.universeId) )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/phase/start", (ev)=>this.doStartPhase( parseInt(ev.props.universeId), parseInt(ev.props.phaseId) )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/action/validate", (ev)=>this.doValidateAction( parseInt(ev.props.universeId), ev.props.actionId )  );
        // PubSub.sub(  "/micro/admin/universe/:universeId/action/fail", (ev)=>this.doFailAction( parseInt(ev.props.universeId), ev.props.actionId )  );
        PubSub_1.default.subAll((ev) => this.onMessageReceived(ev));
        PubSub_1.default.connected(() => {
            this.sendGameState();
        });
        this.gameState = this.gameStateInit();
        setInterval(() => this.onTick(), 1000);
    }
    onMessageReceived(ev) {
        if (ev.topic.startsWith("/admin/")) {
        }
        else if (ev.topic.startsWith("/micro/admin/")) {
            let sub = ev.topic.split("/");
            try {
                if (ev.topic.startsWith("/micro/admin/session/")) {
                    switch (sub[4]) {
                        case "reset":
                            this.doResetSession();
                            break;
                        // case "stop" : 
                        //     this.doEndGame();
                        // break;
                    }
                }
                if (ev.topic.startsWith("/micro/admin/game/")) {
                    switch (sub[4]) {
                        case "start":
                            this.doStartGame();
                            break;
                        case "stop":
                            this.doEndGame();
                            break;
                    }
                }
                if (ev.topic.startsWith("/micro/admin/universe/")) {
                    let universeId = parseInt(sub[4]);
                    switch (sub[5]) {
                        case "start":
                            this.doStartUniverse(universeId);
                            break;
                        case "end":
                            this.doEndUniverse(universeId);
                            break;
                        case "phase":
                            switch (sub[6]) {
                                case "start":
                                    this.doStartPhase(universeId, parseInt(ev.payload));
                                    break;
                                case "end":
                                    this.doEndPhase(universeId);
                                    break;
                            }
                            break;
                        case "action":
                            switch (sub[6]) {
                                case "validate":
                                    this.doValidateAction(universeId, ev.payload);
                                    break;
                                case "fail":
                                    this.doFailAction(universeId, ev.payload);
                                    break;
                            }
                            break;
                    }
                }
            }
            catch (ex) {
                console.error("failled to read admin topic due to : ", ex);
            }
        }
        else {
            this.testActions(ev);
        }
    }
    testActions(ev) {
        if (ev.topic.startsWith("/micro/coffre/")) {
            this.onCardScanEvent(ev);
            return;
        }
        for (const universe of universesIds) {
            let state = this.universeState(universe);
            let actions = this.getPhaseActions(universe, state.currentPhase);
            //console.log("testActions", universe, JSON.stringify(actions))
            for (const action of actions) {
                if (action.state.done) {
                    continue;
                }
                let definition = action.definition;
                let validationTopic = definition.validationTopic;
                let validationRequestedValue = definition.validationRequestedValue;
                switch (action.definition.validationType) {
                    case "SIGNAL": {
                        console.debug("SIGNAL", '[' + validationTopic + '==' + ev.topic + ']', "value", '[' + validationRequestedValue + '==' + ev.payload.trim() + ']');
                        if (validationTopic == ev.topic) {
                            if (!validationRequestedValue
                                || validationRequestedValue == ev.payload.trim()) {
                                this.doValidateAction(universe, action.definition.id);
                            }
                        }
                        break;
                    }
                    case "SEQUENCE": {
                        if (validationTopic == ev.topic && validationRequestedValue) {
                            action.state.sequence += ev.payload.trim();
                            console.debug("SEQUENCE", '[' + validationTopic + '==' + ev.topic + ']', "value", '[' + validationRequestedValue + '==' + ev.payload.trim() + ']', action.state.sequence);
                            if (action.state.sequence.endsWith(validationRequestedValue)) {
                                this.doValidateAction(universe, action.definition.id);
                            }
                            else {
                                if (action.state.sequence.length > validationRequestedValue.length) {
                                    action.state.sequence = action.state.sequence.substring(-validationRequestedValue.length);
                                }
                            }
                        }
                        break;
                    }
                    case "CUSTOM": {
                        //NOPE
                        break;
                    }
                }
            }
        }
    }
    onCardScanEvent(ev) {
        let universe = 1 - parseInt(ev.topic.split("/")[3]);
        let state = this.universeState(universe);
        let currentScanActions = this.getPhaseActions(universe, state.currentPhase).filter(a => a.definition.validationType == "SCAN");
        console.log("currentScanActions", currentScanActions);
        let correct = false;
        let badgeId = ev.payload.slice(-8, -2);
        console.log("badgeId [" + badgeId + "]");
        for (let action of currentScanActions) {
            if (action.definition.validationRequestedValue
                && action.definition.validationRequestedValue.indexOf(badgeId) != -1) {
                correct = true;
                this.doValidateAction(universe, action.definition.id);
            }
        }
        if (!correct) {
            this.muteScan(universe);
        }
    }
    muteScan(universe) {
        if (this.screenStates.getCoffre(universe)) {
            this.screenStates.setCoffre(universe, (state) => { return Object.assign({}, state, { scanActive: false, scanInactiveTimestamp: Date.now() + 30 * 1000 }); });
        }
    }
    /////////////////////
    // GENERAL
    reset() {
        this.init();
    }
    doResetSession() {
        console.debug("doResetSession");
        this.gameState = this.gameStateInit();
        PubSub_1.default.pub(conf_1.default.mqttTopic_salon_tv_mode, "on");
        // TODO Open coffres
        // TODO veille thermostat
        this.sendGameState();
    }
    doStartGame() {
        console.debug("doStartGame");
        this.gameState.gameEndTimeStamp = Date.now() + gameDuration;
        this.gameState.phase = "GAME";
        this.sendGameState();
        this.onStartGame();
    }
    doEndGame() {
        console.debug("doEndGame");
        this.gameState.phase = "END";
        this.sendGameState();
        this.onEndGame();
    }
    checkGameState() {
        if (this.gameState.phase == "GAME" && this.isGameSuccessful()) {
            this.doEndGame();
        }
    }
    onStartGame() {
        this.sendGameTimestamp(this.gameState.gameEndTimeStamp);
        this.sendGamePhase(this.gameState.phase);
    }
    onEndGame() {
        // WOWOWOWOWOW Trop de truc. A verifier avec tout le monde
        this.sendGamePhase("end");
        this.sendGameState();
    }
    onTick() {
        let now = Date.now();
        if (this.isGameRunning()) {
            for (const universeId of universesIds) {
                let state = this.universeState(universeId);
                if (state.currentPhaseEndTimeStamp && now > state.currentPhaseEndTimeStamp) {
                    this.doEndPhase(universeId);
                }
            }
            if (now > this.gameState.gameEndTimeStamp) {
                this.doEndGame();
            }
        }
        for (const universeId of universesIds) {
            let coffreState = this.screenStates.getCoffre(universeId);
            if ((!coffreState.scanActive) && now > coffreState.timestamp) {
                this.screenStates.setCoffre(universeId, (state) => { return Object.assign({}, state, { scanActive: true }); });
            }
        }
    }
    isGameRunning() {
        switch (this.gameState.phase) {
            case "GAME":
                return true;
            case "IDLE":
            case "END":
            case "INTRO":
            default:
                return false;
        }
    }
    universeState(universe) {
        return this.gameState.universes[universe];
    }
    isGameSuccessful() {
        let success = true;
        for (const universeId of universesIds) {
            let universe = this.universeState(universeId);
            if (universe.done == false) {
                success = false;
            }
        }
        return success;
    }
    /////////////////////
    // UNIVERSE
    doStartUniverse(universe) {
        console.debug("doStartUniverse", universe);
        this.doStartPhase(universe, 1);
        let universeState = this.universeState(universe);
        universeState.done = false;
        console.debug("  universeState", universeState);
        this.sendGameState();
    }
    doEndUniverse(universe) {
        console.debug("doEndUniverse", universe);
        this.universeState(universe).done = true;
        this.checkGameState();
        this.sendGameState();
    }
    /////////////////////
    // PHASES
    doStartPhase(universe, phaseId) {
        console.debug("doStartPhase", universe, phaseId);
        let universeState = this.universeState(universe);
        if (universeState.currentPhase != phaseId) {
            universeState.currentPhase = phaseId;
            let phaseDuration = this.getPhaseDuration(universe, phaseId);
            let phaseEndTimestamp = Date.now() + phaseDuration * 1000 * 60;
            universeState.currentPhaseEndTimeStamp = phaseEndTimestamp;
            this.onStartPhase(universe, phaseId);
            for (const action of this.getPhaseActions(universe, phaseId)) {
                this.onActionEnabled(universe, action);
            }
        }
        this.sendGameState();
    }
    onActionEnabled(universe, action) {
        if (action.definition.onStartTopic) {
            PubSub_1.default.pub(action.definition.onStartTopic, action.definition.onStartMessage);
        }
    }
    doEndPhase(universe) {
        console.debug("doEndPhase");
        let universeState = this.universeState(universe);
        let lastPhase = this.getPhaseCount(universe);
        this.onEndPhase(universe, universeState.currentPhase);
        if (universeState.currentPhase == lastPhase) {
            if (this.areActionDone(universeState.actions)) {
                this.doEndUniverse(universe);
            }
            else {
                this.doStartPhase(universe, UNIVERSE_PHASE_COMEBACK);
            }
        }
        else if (universeState.currentPhase == UNIVERSE_PHASE_COMEBACK) {
            if (this.areActionDone(universeState.actions)) {
                this.doEndUniverse(universe);
            }
        }
        else {
            this.doStartPhase(universe, this.universeState(universe).currentPhase + 1);
        }
        this.sendGameState();
    }
    checkPhase(universe) {
        let universeState = this.universeState(universe);
        let phaseId = universeState.currentPhase;
        let isPhaseDone = this.isPhaseDone(universe, phaseId);
        Logger_1.default.debug("checkPhase", universe, phaseId, isPhaseDone);
        if (isPhaseDone) {
            this.doEndPhase(universe);
        }
    }
    getPhaseCount(universe) {
        let phases = this.universeState(universe).actions.map(action => action.definition.phaseId);
        return Math.max(...phases);
    }
    getPhaseActions(universe, phaseId) {
        if (phaseId == UNIVERSE_PHASE_COMEBACK) {
            return this.universeState(universe).actions;
        }
        else {
            return this.universeState(universe).actions.filter(action => action.definition.phaseId == phaseId);
        }
    }
    isPhaseDone(universe, phaseId) {
        let actions = this.getPhaseActions(universe, phaseId);
        if (phaseId != UNIVERSE_PHASE_COMEBACK) {
            return this.areActionDone(actions);
        }
        else {
            return this.areActionSuccessful(actions);
        }
    }
    areActionDone(actions) {
        for (const action of actions) {
            if (!action.state.done) {
                return false;
            }
        }
        return true;
    }
    areActionSuccessful(actions) {
        for (const action of actions) {
            if (!action.state.success) {
                return false;
            }
        }
        return true;
    }
    getPhaseDuration(universe, phaseId) {
        let actions = this.getPhaseActions(universe, phaseId);
        if (actions) {
            return actions[0].definition.phaseDuration;
        }
        return -1;
    }
    onStartPhase(universe, phaseId) {
        this.sendScanMode(universe, true);
        this.sendPhaseTimestamp(universe, this.gameState.universes[universe].currentPhaseEndTimeStamp || this.gameState.gameEndTimeStamp);
    }
    onEndPhase(universe, phaseId) {
        this.sendActiveActions(universe, []);
    }
    /////////////////////
    // ACTIONS
    doValidateAction(universe, actionId) {
        console.debug("doValidateAction");
        let action = this.getAction(universe, actionId);
        if (!action) {
            Logger_1.default.error("validateAction : action " + actionId + " not found in universe " + universe);
            return;
        }
        action.state.done = true;
        action.state.success = true;
        this.onActionComplete(universe, actionId);
        this.sendGameState();
    }
    doFailAction(universe, actionId) {
        console.debug("doFailAction");
        let action = this.getAction(universe, actionId);
        if (!action) {
            Logger_1.default.error("validateAction : action " + actionId + " not found in universe " + universe);
            return;
        }
        action.state.done = true;
        action.state.success = false;
        this.onActionFailled(universe, actionId);
        this.sendGameState();
    }
    getAction(universe, actionId) {
        let universeState = this.universeState(universe);
        let action = universeState.actions.find(a => a.definition.id == actionId);
        return action;
    }
    jaugeCurrentValue(universe) {
        let actions = this.universeState(universe).actions.filter(action => !action.state.done);
        return actions.reduce((p, c) => { return p + c.definition.value; }, 0);
    }
    jaugeTotalValue(universe) {
        let actions = this.universeState(universe).actions;
        return actions.reduce((p, c) => { return p + c.definition.value; }, 0);
    }
    onScanRequest(universe) {
        this.sendScanMode(universe, false);
        let state = this.universeState(universe);
        setTimeout(() => this.sendActiveActions(universe, this.getPhaseActions(universe, state.currentPhase)), sendPinDelay);
    }
    onActionComplete(universe, actionId) {
        let action = this.getAction(universe, actionId);
        this.sendActionComplete(universe, actionId);
        this.sendUpdateJauge(universe, this.jaugeCurrentValue(universe), this.jaugeTotalValue(universe));
        this.checkPhase(universe);
        if (action && action.definition.onEndTopic) {
            PubSub_1.default.pub(action.definition.onEndTopic, action.definition.onEndMessage);
        }
    }
    onActionFailled(universe, actionId) {
        this.sendActionFailled(universe, actionId);
        this.sendUpdateJauge(universe, this.jaugeCurrentValue(universe), this.jaugeTotalValue(universe));
        this.checkPhase(universe);
    }
    //////////////////
    // Messagers
    sendActiveActions(universe, actions) {
        PubSub_1.default.pub(conf_1.default.mqttTopic_ScanPin(universe), JSON.stringify(actions));
    }
    sendScanMode(universe, on) {
        PubSub_1.default.pub(conf_1.default.mqttTopic_ScanActivationState(universe), on ? "1" : "0");
    }
    sendActionComplete(universe, actionId) {
        PubSub_1.default.pub(conf_1.default.mqttTopic_ActionComplete(universe, actionId));
    }
    sendActionFailled(universe, actionId) {
        PubSub_1.default.pub(conf_1.default.mqttTopic_ActionFailled(universe, actionId));
    }
    sendPhaseTimestamp(universe, timestamp) {
        PubSub_1.default.pub(conf_1.default.mqttTopic_PhaseTimestamp(universe), timestamp.toString());
    }
    sendGameTimestamp(timestamp) {
        PubSub_1.default.pub(conf_1.default.mqttTopic_GameTimestamp(), timestamp.toString());
    }
    sendUpdateJauge(universe, jaugeValue, jaugeTotal) {
        PubSub_1.default.pub(conf_1.default.mqttTopic_JaugeValue(universe), [jaugeValue, jaugeTotal].join(" "));
    }
    sendGamePhase(gamePhase) {
        PubSub_1.default.pub(conf_1.default.mqttTopic_GamePhase(), gamePhase);
    }
    sendGameState() {
        PubSub_1.default.pub(conf_1.default.mqttTopic_GameState(), JSON.stringify(this.gameState));
    }
}
exports.default = new GameEngine();
//# sourceMappingURL=GameEngine.js.map