"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const PubSub_1 = tslib_1.__importDefault(require("../controllers/PubSub"));
class ScanState {
    constructor() {
        this.phase = "idle";
        this.scanEnabled = false;
        this.scanning = false;
        this.pins = [];
        this.message = "";
        this.timestamp = 0;
    }
    ;
}
class DbState {
    constructor() {
        this.phase = "idle";
        this.message = "";
    }
}
class CoffreState {
    constructor() {
        this.phase = "message";
        this.timestamp = 0;
        this.jaugeValue = 100;
        this.jaugeMax = 100;
        this.scanActive = false;
        this.message = "Vérouillé";
    }
}
class ThermostatState {
    constructor() {
        this.phase = "sleep";
        this.temperature = 0;
    }
}
class PCState {
    constructor() {
        this.phase = "sleep";
        this.temperature = 0;
    }
}
class TVState {
    constructor() {
        this.phase = "video";
    }
}
class ScreenStates {
    constructor() {
        this._scans = [
            new ScanState(),
            new ScanState()
        ];
        this._dbs = [
            new DbState(),
            new DbState(),
        ];
        this._coffres = [
            new CoffreState(),
            new CoffreState(),
        ];
        // private _thermostats:ThermostatState[] = [
        //     new ThermostatState(),
        //     new ThermostatState(),
        // ]
        // private _pc:PCState = new PCState;
        // private _tv:TVState = new TVState;
        // getThermostat(id:Universe){
        //     return this._thermostats[id];
        // }
        // setThermostat(id:Universe, value:ThermostatState){
        //     this._thermostats[id] = value;
        //     PubSub.pub("/states/thermostat/"+id, JSON.stringify(value));
        // }
        // get pc(){
        //     return this._pc;
        // }
        // set pc(value = this.pc){
        //     this._pc;
        //     PubSub.pub("/states/thermostat/"+id, JSON.stringify(value));
        // }
    }
    getScan(id) {
        return this._scans[id];
    }
    getDb(id) {
        return this._dbs[id];
    }
    getCoffre(id) {
        return this._coffres[id];
    }
    setScan(id, action) {
        this._scans[id] = action(this._scans[id]);
        ;
        PubSub_1.default.pub("/states/scan/" + id, JSON.stringify(this._scans[id]));
    }
    setDb(id, action) {
        this._dbs[id] = action(this._dbs[id]);
        PubSub_1.default.pub("/states/db/" + id, JSON.stringify(this._dbs[id]));
    }
    setCoffre(id, action) {
        this._coffres[id] = action(this._coffres[id]);
        PubSub_1.default.pub("/states/coffre/" + id, JSON.stringify(this._coffres[id]));
    }
}
exports.default = ScreenStates;
//# sourceMappingURL=ScreenStates.js.map